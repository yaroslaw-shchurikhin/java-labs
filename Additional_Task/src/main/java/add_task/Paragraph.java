package add_task;

import java.util.List;

public class Paragraph implements Markup{

    private final List<? extends Markup> data_;

    public Paragraph(List<? extends Markup> elements) {
       data_ = elements;
    }

    @Override
    public void toMarkdown(StringBuilder sb){
        data_.forEach(x -> x.toMarkdown(sb));
        System.out.println(sb);
    }
}
