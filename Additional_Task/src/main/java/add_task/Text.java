package add_task;

public class Text implements Markup{
    private final String data_;

    public Text(String text){
        data_ = text;
    }

    @Override
    public void toMarkdown(StringBuilder sb){
        sb.append(data_);
    }
}
