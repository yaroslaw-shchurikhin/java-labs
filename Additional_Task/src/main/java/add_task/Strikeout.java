package add_task;

import java.util.List;

public class Strikeout implements Markup {

    private final List<? extends Markup> data_;

    public Strikeout(List<? extends Markup> elements) {
        data_ = elements;
    }

    @Override
    public void toMarkdown(StringBuilder sb){
        sb.append("~");
        data_.forEach(x -> x.toMarkdown(sb));
        sb.append("~");
    }
}
