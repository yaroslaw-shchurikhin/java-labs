package add_task;

import java.util.List;

public class Strong implements Markup{

    private final List<? extends Markup> data_;

    public Strong(List<? extends Markup> elements) {
        data_ = elements;
    }

    @Override
    public void toMarkdown(StringBuilder sb){
        sb.append("__");
        data_.forEach(x -> x.toMarkdown(sb));
        sb.append("__");
    }
}
