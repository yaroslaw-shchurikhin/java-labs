package add_task;

import java.util.List;

public class Emphasis implements Markup{

    private final List<? extends Markup> data_;

    public Emphasis(List<? extends Markup> elements) {
        data_ = elements;
    }

    @Override
    public void toMarkdown(StringBuilder sb){
        sb.append("*");
        data_.forEach(x -> x.toMarkdown(sb));
        sb.append("*");
    }
}
