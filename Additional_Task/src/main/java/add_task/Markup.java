package add_task;

public interface Markup {
     void toMarkdown(StringBuilder sb);
}
