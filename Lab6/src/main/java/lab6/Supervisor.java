package lab6;

public class Supervisor {

    private final Abstract_program abstract_program_;
    private Thread thread_program_;

    public Supervisor(Abstract_program abstract_program){
        abstract_program_ = abstract_program;

    }

    private void start() {
        System.out.println("Start program");
        thread_program_ = new Thread(abstract_program_);
        thread_program_.start();
    }

    private void unpause() {
        System.out.println("Unpause program");
        abstract_program_.stirUp();
    }

    private void restart() {

        System.out.println("Restart program");
        stop();
        start();
    }

    private void stop() {
        System.out.println("Stop program");
        unpause();
        thread_program_.interrupt();
    }

    public void trackProgram(){
        start();

        while(true) {

            unpause();
            try {
                synchronized (this) {
                    this.wait(1000);
                }
            } catch (InterruptedException ignored) {}

            System.out.println(abstract_program_.getStatus());

            Status st = abstract_program_.getStatus();

            if(st == Status.UNKNOWN || st == Status.STOPPING){
                restart();
            }
            else if(st == Status.FATAL_ERROR){
                stop();
                break;
            }
        }
    }
}
