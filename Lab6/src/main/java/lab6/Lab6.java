package lab6;

public class Lab6 {
    public static void main(String[] args) {
        Abstract_program ap = new Abstract_program();

        Supervisor sv = new Supervisor(ap);
        sv.trackProgram();
    }
}
