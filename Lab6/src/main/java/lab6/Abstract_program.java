package lab6;

import java.util.concurrent.ThreadLocalRandom;

public class Abstract_program implements Runnable{

    Status programStatus = Status.UNKNOWN;
    private int random;

    public Status getStatus(){
        return programStatus;
    }

    public synchronized void sleep(){
        System.out.println("Pause program");

        try {
            wait();
        }catch(InterruptedException ignored){}

    }

    public synchronized void stirUp(){
        notify();
    }

    public void run(){
        System.out.println("Start abstract program");

        Thread daemon = new Thread(() ->
        {
            while(true)
            {
                random = (ThreadLocalRandom.current().nextInt(0, 1000) % 4);
            }
        });

        daemon.setDaemon(true);
        daemon.start();

        while(!Thread.currentThread().isInterrupted())
        {

            switch (random) {
                case 0 -> programStatus = Status.UNKNOWN;
                case 1 -> programStatus = Status.STOPPING;
                case 2 -> programStatus = Status.RUNNING;
                case 3 -> programStatus = Status.FATAL_ERROR;

            }

            sleep();

            synchronized (Thread.currentThread()) {
                try {
                    Thread.currentThread().wait(100);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }

            if(Thread.currentThread().isInterrupted())
            {
                System.out.println("Abstract program was interrupted");
                break;
            }
        }
    }
}

enum Status{
    UNKNOWN,
    STOPPING,
    RUNNING,
    FATAL_ERROR
}