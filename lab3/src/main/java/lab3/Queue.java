package lab3;

public class Queue<T> {

    private final int size;
    private final Object[] data;

    private int head = 1;
    private int tail = 1;

    public Queue(int size_){

        size = size_;
        data = new Object[size+1];
    }

    public boolean empty()
    {
        return head == tail;
    }

    public int size(){
        return size;
    }

    public void add(T elem) throws QueueOverflowException{

        if(head == tail + 1){
            throw new QueueOverflowException("\nThe queue is full!\n");
        }

        data[tail] = elem;
        ++tail;

        if(tail == size + 1){
            tail = 0;
        }
    }

    public T get() throws QueueUnderflowException{

        if(head == tail){
            throw new QueueUnderflowException("\nThe queue is empty!\n");
        }

        T item = (T) data[head];
        ++head;

        if(head == size + 1){
            head = 0;
        }

        return item;
    }

    public T peek(){

        if(head == tail){
            return null;
        }

        return (T) data[head];
    }
}
