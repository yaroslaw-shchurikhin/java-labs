package lab3;

import java.lang.reflect.Type;

public class Lab3 {
    public static void main(String[] args) {

            Queue<? super Aluterus_Monoceros> animals = consume(produce());

            try {
                for (int i = 0; i < animals.size(); ++i) {

                    System.out.println(animals.get().getClass());
                }
            }
            catch(QueueUnderflowException ex){

                System.out.println(ex.getMessage());
            }
    }

    public static Queue<? extends Chordal> produce(){ //наследники позвоночных

        Birds b_class = new Birds();
        Actinopterygii a_class = new Actinopterygii();
        Woodpeckers w_detachment = new Woodpeckers();
        Tetraodontiformes t_detachment = new Tetraodontiformes();
        Tukan t_family = new Tukan();
        Monacanthidae m_family = new Monacanthidae();
        Aulacorhynchus au_gender = new Aulacorhynchus();
        Aluterus al_gender = new Aluterus();
        Aluterus_Monoceros am_kind = new Aluterus_Monoceros();
        Aluterus_Scriptus as_kind = new Aluterus_Scriptus();
        Aulacorhynchus_Derbianus ad_kind = new Aulacorhynchus_Derbianus();
        Aulacorhynchus_Prasinus ap_kind = new Aulacorhynchus_Prasinus();

        Queue<Chordal> chordalQueue = new Queue<>(12);

        try {
            chordalQueue.add(b_class);
            chordalQueue.add(a_class);
            chordalQueue.add(w_detachment);
            chordalQueue.add(t_detachment);
            chordalQueue.add(t_family);
            chordalQueue.add(m_family);
            chordalQueue.add(au_gender);
            chordalQueue.add(al_gender);
            chordalQueue.add(am_kind);
            chordalQueue.add(as_kind);
            chordalQueue.add(ad_kind);
            chordalQueue.add(ap_kind);

        }
        catch(QueueOverflowException ex){

            System.out.println(ex.getMessage());
        }

        return chordalQueue;
    }

    public static Queue<? super Aluterus_Monoceros> consume(Queue<? extends Chordal> chordalQueue) {

        Queue<? super Chordal> am_parents = new Queue<>(4);

        Aluterus_Monoceros am_example = new Aluterus_Monoceros();

        try {
            for (int i = 0; i < chordalQueue.size(); ++i) {
                var queueElem = chordalQueue.get();

                if (queueElem.getClass().isAssignableFrom(am_example.getClass())
                && !(queueElem instanceof Aluterus_Monoceros)) { //Родители Aluterus_Monoceros

                    am_parents.add(queueElem);
                }
            }

        } catch (QueueUnderflowException | QueueOverflowException ex) {
            System.out.println(ex.getMessage());
        }

        return am_parents;
    }

}
