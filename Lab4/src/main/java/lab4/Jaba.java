package lab4;

public class Jaba {

    @Repeater(count = 1)
    private boolean method1(){

        System.out.println("Method1");
        return true;
    }

    @Repeater(count = 5)
    private void method2(){
        System.out.println("Method2");
    }

    @Repeater(count = 3)
    private int method3(){

        System.out.println("Method3");
        return 50;
    }
}
