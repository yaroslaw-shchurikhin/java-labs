package lab4;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;

public class Lab4 {

    public static void main(String[] args) {
        Jaba alla = new Jaba();

        var className = alla.getClass();
        var methods  = className.getDeclaredMethods();

        for(var method : methods) {
            Annotation[] annotations = method.getAnnotations();

            for (var anno : annotations) {
                
                if (anno.annotationType() == Repeater.class) {
                    Repeater rep = (Repeater) anno;

                    try {
                        for(int i = 0; i < rep.count(); ++i) {
                            method.trySetAccessible();
                            method.invoke(alla);
                        }

                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
}
