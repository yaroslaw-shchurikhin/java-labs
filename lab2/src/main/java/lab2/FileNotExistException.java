package lab2;

public class FileNotExistException extends Exception{

    private String name;

    public String getName(){
        return name;
    }

    public FileNotExistException(String message, String nameOfFile){

        super(message);
        name = nameOfFile;
    }
}
