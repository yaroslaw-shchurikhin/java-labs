package lab2;

import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

public class Matrix {

    private int size;
    private double[][] data;

    public Matrix(int size_, double[][] data_) {

        size = size_;
        data = data_;
    }

    public Matrix(int size_) {
        size = size_;
        data = new double[size][size];


        for (int i = 0; i < size; ++i) {

            for (int j = 0; j < size; ++j) {

                data[i][j] = ThreadLocalRandom.current().nextDouble(-size, size);
            }
        }
    }

    public Matrix(Matrix m) {
        size = m.size;
        data = new double[size][size];

        for (int i = 0; i < size; ++i) {

            for (int j = 0; j < size; ++j) {

                data[j][i] = m.data[i][j];
            }
        }
    }

    public void twistMatrix() {

        for(int i = 0; i < 3; ++i){

            this.ninetyDegreeRotation();
            this.print();
        }

        this.divideElementsWithTheSumOfNeighbors();
    }

    private void ninetyDegreeRotation()
    {
        double[][] new_data = new double[size][size];

        for (int i = 0; i < size; ++i) {

            int top = 2;
            for (int j = 0; j < size; ++j) {

                new_data[j][i] = data[i][j + top];
                top -= 2;
            }
        }

        data = new_data;
    }

    private void divideElementsWithTheSumOfNeighbors()
    {
        double[][] new_data = new double[size][size];
        try {

            for (int i = 0; i < size; ++i) {
                for (int j = 0; j < size; ++j) {

                    double sum = sumOfNeighbours(i, j);

                    if (sum != 0) {

                        new_data[i][j] = (data[i][j] / sumOfNeighbours(i, j));
                    } else {

                        throw new DivisionByZeroException("\nDivision by zero in Matrix.java. " +
                                "The conversion will not be performed\n", i, j);
                    }
                }
            }
        }
        catch(DivisionByZeroException ex){

            System.out.println(ex.getMessage());
            return;
        }

        data = new_data;
    }

    private double sumOfNeighbours(int i, int j)
    {
        if(i == 0){
            if(j == 0){
               return data[0][1] + data[1][0] ;
            }
            else if(j == size-1){
                return data[0][j-1] + data[1][j];
            }
            else{
                return data[0][j-1] + data[0][j+1] + data[1][j];
            }
        }

        else if(i == size-1){
            if(j == 0){
                return data[i-1][0] + data[i][1];
            }
            else if(j == size-1){
                return data[i][j-1] + data[i-1][j];
            }
            else{
                return data[i][j-1] + data[i][j+1] + data[i-1][j];
            }
        }

        else{
            if(j == 0){
                return data[i-1][0] + data[i+1][0] + data[i][1];
            }
            else if(j == size-1){
                return data[i][j-1] + data[i+1][j] + data[i-1][j];
            }
            else{
                return data[i][j-1] + data[i][j+1] + data[i+1][j] + data[i-1][j];
            }
        }
    }

    public void print() {

        for(int i = 0; i < size; ++i) {
            for(int j = 0; j < size; ++j) {

                System.out.printf("%5.2f", data[i][j]);
                System.out.print(' ');
            }
            System.out.print('\n');
        }

    }

    public void print(FileWriter fout) throws IOException {

        for(int i = 0; i < size; ++i) {
            for(int j = 0; j < size; ++j) {

                fout.write(Double.toString(data[i][j])); ///вывод с помощью printf
                fout.append(' ');
            }

            fout.append('\n');
        }
        fout.append('\n');
    }
}
