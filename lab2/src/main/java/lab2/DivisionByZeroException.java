package lab2;

public class DivisionByZeroException extends ArithmeticException{

    private int i1; //индексы элемента, который пытается поделить на 0
    private int i2;

    public Pair<Integer, Integer> getIndexes() {
        return new Pair<>(i1, i2);
    }

    public DivisionByZeroException(String message, int index1, int index2){
        super(message);

        i1 = index1;
        i2 = index2;
    }
}
