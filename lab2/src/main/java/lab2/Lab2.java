package lab2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Lab2 {

    public static void main(String[] args) {

        try {
            System.out.println("Введите размер матрицы");
            Scanner console = new Scanner(System.in);
            int N = 0;

            try {
                if (console.hasNextInt()) {

                    N = console.nextInt();

                    if (N > 1000000) {

                        throw new TooBigSizeException("\nSize of Matrix is too big!\n", N);
                    } else if (N <= 0) {

                        throw new IllegalArgumentException("\nSize of Matrix must be a positive number!\n");
                    }

                } else {

                    throw new IllegalArgumentException("\nSize of Matrix must be a positive number!\n");
                }

            } catch (IllegalArgumentException | TooBigSizeException ex) {

                System.out.println(ex.getMessage());
                return;
            }

            System.out.println("\nВведите имя файла");
            String inputFile = console.next();

            double[][] matrixData = new double[N][N];

            try {
                File inFile = new File(inputFile);

                if (!inFile.exists()) {

                    throw new FileNotExistException("\nCan't open this file\n", inputFile);
                }

                FileReader fin = new FileReader(inFile);
                matrixData = readMatrix(N, fin);

            } catch (IOException | FileNotExistException | IllegalArgumentException ex) {

                System.out.println(ex.getMessage()); //printStackTrace
                return;
            }

            Matrix m1 = new Matrix(N, matrixData);

            Matrix m2 = new Matrix(N);

            Matrix m3 = new Matrix(m1);

            m3.twistMatrix();

            String outputFile = "C:\\java_tests\\output.txt";

            try {
                File outFile = new File(outputFile);

                if (outFile.exists()) {

                    FileWriter fout = new FileWriter(outFile);
                    fout.write("Matrix #1\n");
                    m1.print(fout);

                    fout.write("Matrix #2\n");
                    m2.print(fout);

                    fout.write("Matrix #3\n");
                    m3.print(fout);

                    fout.close();

                } else {

                    throw new FileNotExistException("\nCan't open this file\n", outputFile);
                }
            } catch (IOException| FileNotExistException ex) {

                System.out.println(ex.getMessage());
                return;

            }
        } catch (OutOfMemoryError err) {

            System.out.println(err.getMessage());
        }
    }

        public static double[][] readMatrix ( int size, FileReader fin)
        {
            Scanner file = new Scanner(fin);

            double[][] matrixData = new double[size][size];

            for (int i = 0; i < size; ++i) {
                for (int j = 0; j < size; ++j) {

                    if (file.hasNextDouble() && file.hasNext()) {

                        matrixData[i][j] = file.nextDouble();
                    } else if (file.hasNext()) {

                        file.next();
                        --j;
                    } else {

                        throw new IllegalArgumentException("\nIncorrect input data: Insufficient data\n");
                    }
                }
            }

            return matrixData;
        }
}
