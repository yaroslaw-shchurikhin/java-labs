package lab2;

public class TooBigSizeException extends Exception{

    private int size;

    public int getSize(){

        return size;
    }

    public TooBigSizeException(String message, int size_){

        super(message);
        size = size_;
    }
}
