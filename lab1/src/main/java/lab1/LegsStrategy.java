package lab1;

public class LegsStrategy implements MoveStrategy {

    @Override
    public Point move(Point start, Point end) {

        int walking_speed = 6;
        double distance = Math.sqrt(Math.pow(start.getX() - end.getX(), 2)
                + Math.pow(start.getY() - end.getY(), 2));

        double time_h = distance/walking_speed;

        System.out.println("Время пути пешком: " + time_h + " часов");
        return end;
    }
}
