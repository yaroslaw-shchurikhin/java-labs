package lab1;

public class CarStrategy implements MoveStrategy {

    @Override
    public Point move(Point start, Point end) {

        int car_speed = 60;

        double distance = Math.sqrt(Math.pow(start.getX() - end.getX(), 2)
                + Math.pow(start.getY() - end.getY(), 2));

        double time_h = distance/car_speed;

        System.out.println("Время пути на машине: " + time_h + " часов");

        return end;
    }
}
