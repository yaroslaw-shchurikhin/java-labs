package lab1;

public interface MoveStrategy {

    Point move(Point start, Point end);
}
