 package lab1;

public class Lab1 {
    public static void main(String[] args) {

        Hero techies = new Hero();

        Point point1 = new Point(2F, 2F);
        techies.move(point1);

        techies.getOnTheHorse();

        Point point2 = new Point(189F, -120F);
        techies.move(point2);

        techies.getInTheCar();

        Point point3= new Point(0F, 0F);
        techies.move(point3);

        techies.getInThePlane();

        Point point4 = new Point(899F, 9999F);
        techies.move(point4);

    }
}
