package lab1;

import java.util.Objects;

public class Hero {

    private int hp;
    private Point loc;
    private MoveStrategy strategy = new LegsStrategy();

    public Hero()
    {
        hp = 0;
        loc = new Point(0F,0F);
    }

    public Hero(int hp_, Point loc_)
    {
        hp = hp_;
        loc = loc_;
    }

    public int getHP() {
        return hp;
    }

    public Point getLoc() {
        return loc;
    }

    public void getInTheCar()
    {
        if(!Objects.equals(strategy, new CarStrategy()))
        strategy = new CarStrategy();
    }

    public void getInThePlane()
    {
        if(!Objects.equals(strategy, new PlaneStrategy()))
            strategy = new PlaneStrategy();
    }

    public void getOnTheHorse()
    {
       if(!Objects.equals(strategy, new HorseStrategy()))
        strategy = new HorseStrategy();
    }

    public void dismount()
    {
        if(!Objects.equals(strategy, new LegsStrategy()))
            strategy = new LegsStrategy();
    }

    public void move(Point start, Point end) {
        loc = strategy.move(start, end);
    }

    public void move(Point end) {
        loc = strategy.move(loc, end);
    }
}
