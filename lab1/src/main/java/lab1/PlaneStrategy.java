package lab1;

public class PlaneStrategy implements MoveStrategy {

    @Override
    public Point move(Point start, Point end) {

        int plane_speed = 800;

        double distance = Math.sqrt(Math.pow(start.getX() - end.getX(), 2)
                + Math.pow(start.getY() - end.getY(), 2));

        double time_h = distance/plane_speed;
        System.out.println("Время пути на самолёте: " + time_h + " часов");

        return end;
    }

}
