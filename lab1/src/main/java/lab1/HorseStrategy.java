package lab1;

public class HorseStrategy implements MoveStrategy {

    @Override
    public Point move(Point start, Point end) {

        int horse_speed = 88;
        double distance = Math.sqrt(Math.pow(start.getX() - end.getX(), 2)
                + Math.pow(start.getY() - end.getY(), 2));

        double time_h = distance/horse_speed;

        System.out.println("Время пути на лошади: " + time_h + " часов");
        return end;
    }
}
