package lab1;

public class Point {

    private float x;
    private float y;

    public Point(float x_, float y_) {
        this.x = x_;
        this.y = y_;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
}
