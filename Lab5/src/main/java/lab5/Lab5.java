package lab5;

import java.util.*;
import java.util.stream.Collectors;

public class Lab5 {
    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>();
        numbers.add(4);
        numbers.add(11);
        numbers.add(48);
        numbers.add(-124);
        numbers.add(-1299);

        System.out.println("Task 1:");
        System.out.println(task1(numbers));

        System.out.println();

        List<String> strings = new ArrayList<>();
        strings.add("cat");
        strings.add("Billy");
        strings.add("deFenCe");

        System.out.println("Task 2:");
        task2(strings)
                .forEach(System.out::println);

        System.out.println();

        List<Integer> numbers2 = new ArrayList<>();
        numbers2.add(11);
        numbers2.add(11);
        numbers2.add(5);
        numbers2.add(3);
        numbers2.add(1);
        numbers2.add(5);

        System.out.println("Task 3:");
        task3(numbers2)
                .forEach(System.out::println);

        System.out.println();

        List<String> strings2 = new ArrayList<>();
        strings2.add("Tank");
        strings2.add("Billy");
        strings2.add("12");
        strings2.add("Take");
        strings2.add("low");

        System.out.println("Task 4:");

        task4(strings2)
                .forEach(System.out::println);

        System.out.println();

        List<Integer> numbers3 = new ArrayList<>();
        numbers3.add(1);
        numbers3.add(2);
        numbers3.add(3);

        List<Integer> empty = new ArrayList<>();

        System.out.println("Task 5:");

        try {
            System.out.println(task5(numbers3));
            task5(empty);
        }
        catch(EmptyCollectionException ex){
            System.out.println(ex.getMessage());
        }

        System.out.println();

        int[] numbers4 = new int[5];
        numbers4[0] = 2;
        numbers4[1] = 1;
        numbers4[2] = 5;
        numbers4[3] = 34;
        numbers4[4] = 7;

        int[] numbers5 = new int[5];
        numbers5[0] = 11;
        numbers5[1] = 1;
        numbers5[2] = 5;
        numbers5[3] = 49;
        numbers5[4] = 7;

        System.out.println("Task 6:");
        System.out.println(task6(numbers4));
        System.out.println(task6(numbers5));

        System.out.println();

        System.out.println("Task 7:");

        List<String> strings3 = new ArrayList<>();
        strings3.add("joke");
        strings3.add("Billy");
        strings3.add("12");
        strings3.add("Take");
        strings3.add("low");

        printMap(task7(strings3));
    }

    public static double task1(List<Integer> numbers) {

        IntSummaryStatistics stats = numbers.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();


        return stats.getAverage();
    }

    public static List<String> task2(List<String> strings) {

        return strings.stream()
                .map(string -> "_new_" + string.toUpperCase())
                .collect(Collectors.toList());
    }

    public static List<Integer> task3(List<Integer> numbers){

        return numbers.stream()
                .filter(x -> Collections.frequency(numbers, x) == 1)
                .map(x -> x*x)
                .collect(Collectors.toList());
    }

    public static Collection<String> task4(Collection<String> strings){

        char start_symbol = 'T';

        return strings.stream()
                .filter(str -> str.charAt(0) == start_symbol)
                .sorted()
                .collect(Collectors.toList());
    }

    public static <T> T task5(Collection<T> coll) throws EmptyCollectionException{

        Optional<T> opt_result = coll.stream()
                .findAny();

        T result;

        if(opt_result.isPresent()){

            result  = coll.stream()
                    .reduce((x, y) -> y)
                    .get();

            return result;
        }

        else throw new EmptyCollectionException("Collection is empty!");
    }

    public static long task6(int[] numbers){

        return Arrays.stream(numbers)
                .filter(x -> x % 2 == 0)
                .sum();
    }

    public static Map<Character, String> task7(List<String> strings) {

        Map<Character, String> result = new HashMap<>();

        strings
                .forEach(str ->
                        result.put(str.charAt(0), str.substring(1)));

        return result;
    }

    public static void printMap(Map<Character, String> map)
    {
        for(Map.Entry<Character, String> pair : map.entrySet())
        {
            Character key = pair.getKey();
            String value = pair.getValue();
            System.out.println(key + " - " + value);
        }
    }
}
