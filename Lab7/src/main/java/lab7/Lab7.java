package lab7;

import java.util.ArrayList;
import java.util.concurrent.*;

public class Lab7 {
    public static void main(String[] args) {

        int N = Integer.parseInt(args[0]);
        ArrayList<Thread> producers = new ArrayList<>();
        ArrayList<Thread> consumers = new ArrayList<>();

        LinkedBlockingQueue<String> queueOfMessages = new LinkedBlockingQueue<>();

        for(int i = 0; i < N; ++i){

            producers.add(new Thread(() -> {
                for(int j = 0; j < ThreadLocalRandom.current().nextInt(0, 20); ++j){
                    queueOfMessages.add("Hello, i'm " + Thread.currentThread().getName()
                            + " and i want to recommend you new auto");
                }

                queueOfMessages.add(Thread.currentThread().getName() + " УльтраМегаСуперНасрал");
            } ,"Producer-" + i));

            consumers.add(new Thread(() -> {
                do{
                    try {
                        String s = queueOfMessages.take();
                        if(s.contains("Hello, i'm"))
                        System.out.println(Thread.currentThread().getName()
                                + " " + s);
                        else System.out.println(s);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }while(!queueOfMessages.isEmpty());

            } ,"Consumer-" + i));

        }

        producers
                .forEach(Thread::start);
        consumers
                .forEach(Thread::start);

    }
}
